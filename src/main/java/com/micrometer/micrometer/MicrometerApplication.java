package com.micrometer.micrometer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.Duration;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.jmx.JmxMeterRegistry;
import io.micrometer.prometheus.PrometheusMeterRegistry;

@SpringBootApplication
public class MicrometerApplication {

	private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);


	public static void main(String[] args) {
		SpringApplication.run(MicrometerApplication.class, args);
	}



	@Bean
	ApplicationRunner micrometerRunner(MeterRegistry mr) {

		/* COUNTER: counter is a amount of values, so imagine the next scenario
		 * We have an endpoint to products, and when an user enter to watch the product
		 * we can update our counter for this product, so we have the most product visited
		 * in our metrics.
		 */
		mr.counter("orders-placed").increment(1);

		/* GAUGE: gauge shows the current value of a meter. Usually you whant to use them
		 * when monitoring stats of cache, collections, etc.
		 */
		mr.gauge("speed", 55);
		SlowStatefulThing customerService = new SlowStatefulThing();
		mr.gauge("customers-logger-in", customerService, SlowStatefulThing::getCustomerLoggedInToSystem);



		//return args ->
		//	this.executorService
		//			.scheduleWithFixedDelay(() ->
		//					mr.timer("transform-task").record(Duration.ofMillis((long) (Math.random() * 1000)))
		//	, 500, 500, TimeUnit.MILLISECONDS);

		return args ->
			this.executorService
					.scheduleWithFixedDelay(() ->
						Timer
							.builder("transform-task")
							.sla(Duration.ofMillis(1), Duration.ofMillis(10))
							.publishPercentileHistogram()
							.tag("format", Math.random() > .5 ? "JSON" : "XML")
							.register(mr)
							.record(Duration.ofMillis((long) (Math.random() * 1000))
							), 500, 500, TimeUnit.MILLISECONDS);
	}


	@Bean
	MeterRegistryCustomizer<MeterRegistry> registryCustomizer(@Value("${REGION:eu-west") String region) {
		return registry -> registry.config().commonTags("region", region);
	}

	// Disable Metrics by type
	{/*
	//We also can enable or disable metrics in code
	@Bean
	MeterFilter meterFilter() {
		return MeterFilter.denyNameStartsWith("jvm");
	}
	*/}

	@Bean
	JvmThreadMetrics threadMetrics(){
		return new JvmThreadMetrics();
	}
}
